<?php

namespace PatternTwig\Tests;

use PatternTwig\Pattern\Event\IncludeEvent;
use PatternTwig\Pattern\TwigExtension;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Test pattern_twi_extension twig extension.
 *
 * @package PatternTwig\Tests
 */
class PatternEventsTest extends \Twig_Test_IntegrationTestCase {

  public function getExtensions() {
    $dispatcher = new EventDispatcher();
    $dispatcher->addSubscriber(new PatternEventsSubscriber());
    return [
      new TwigExtension($dispatcher),
    ];
  }

  /**
   * @return string
   */
  protected function getFixturesDir() {
    return dirname(__FILE__).'/Fixtures/functions/pattern.events';
  }
}

class PatternEventsSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      IncludeEvent::PRE_INCLUDE => 'preInclude',
      IncludeEvent::POST_INCLUDE => 'postInclude',
    ];
  }

  public function preInclude(IncludeEvent $event) {
    if ($event->getOriginalTemplate() === 'bar.pre.twig') {
      $event->setTemplate('foo.pre.triggered.twig');
    }
  }

  public function postInclude(IncludeEvent $event) {
    if ($event->getOriginalTemplate() === 'foo.post.twig') {
      $event->setOutput($event->getOutput() . 'OVERRIDDEN');
    }
  }
}
