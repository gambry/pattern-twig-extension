<?php

namespace PatternTwig\Tests;

use PatternTwig\Pattern\TwigExtension;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Test pattern_twi_extension twig extension.
 *
 * @package PatternTwig\Tests
 */
class PatternFunctionTest extends \Twig_Test_IntegrationTestCase {

  public function getExtensions() {
    $dispatcher = $this->createMock(EventDispatcherInterface::class);
    return [
      new TwigExtension($dispatcher),
    ];
  }

  /**
   * @return string
   */
  protected function getFixturesDir() {
    return dirname(__FILE__).'/Fixtures/functions/pattern';
  }
}
