<?php

namespace PatternTwig;

use PatternLab\Dispatcher;
use PatternLab\Listener;
use PatternLab\PatternEngine\Twig\TwigUtil;
use PatternTwig\Pattern\TwigExtension;

/**
 * PatternLab Listener.
 *
 * Load the extension on PatternLab.
 *
 * @package PatternTwig
 */
class PatternLabListener extends Listener {

  /**
   * PatternLabListener constructor.
   */
  public function __construct() {
    $this->addListener('twigPatternLoader.customize', 'LoadPattern', 99);
  }

  public function LoadPattern() {
    $env = TwigUtil::getInstance();
    $dispatcher = Dispatcher::getInstance();
    $env->addExtension(new TwigExtension($dispatcher));
  }
}
