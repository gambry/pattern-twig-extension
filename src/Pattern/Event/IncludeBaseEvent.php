<?php

namespace PatternTwig\Pattern\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Base class to help with Pattern inclusion events.
 *
 * @package PatternTwig\Pattern
 */
abstract class IncludeBaseEvent extends Event {

  /**
   * The template name, as passed to pattern() function, if not already altered
   * by other subscribers.
   *
   * @var string
   */
  protected $template;

  /**
   * The original template name, as passed to pattern() function.
   *
   * @var string
   */
  private $originalTemplate;

  /**
   * The variables passed to the pattern() function, if not already altered by
   * other subscribers.
   *
   * @var array
   */
  protected $variables;

  /**
   * The original variables, as passed to pattern() function.
   *
   * @var array
   */
  private $originalVariables;

  /**
   * PreIncludeEvent constructor.
   *
   * @param string $template
   *   The template name.
   * @param array $variables
   *   The template data/variables.
   */
  public function __construct($template, array $variables = []) {
    $this->template = $this->originalTemplate = $template;
    $this->variables = $this->originalVariables = $variables;
  }

  /**
   * @return string
   */
  public function getTemplate(): string {
    return $this->template;
  }

  /**
   * @return array
   */
  public function getVariables(): array {
    return $this->variables;
  }

  /**
   * @return string
   */
  public function getOriginalTemplate(): string {
    return $this->originalTemplate;
  }

  /**
   * @return array
   */
  public function getOriginalVariables(): array {
    return $this->originalVariables;
  }
}
