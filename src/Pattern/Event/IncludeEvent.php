<?php

namespace PatternTwig\Pattern\Event;

/**
 * Event dispatched when a pattern is being included.
 *
 * This event is provided to subscribers both when a template is resolved and
 * rendered (PRE_INCLUDE) and after (POST_COMMIT). Listeners can
 * subscribe to this event for example for alter the template going to be
 * rendered and its variables (PRE_COMMIT) or altering its output before this
 * is included in the parent template (POST_COMMIT).
 *
 * @package PatternTwig\Pattern
 */
class IncludeEvent extends IncludeBaseEvent {

  /**
   * Pre-inclusion event name.
   */
  const PRE_INCLUDE = 'pattern.pre_include';

  /**
   * Post-inclusion event name.
   */
  const POST_INCLUDE = 'pattern.post_include';

  /**
   * The template rendered output.
   *
   * @var string
   */
  protected $output = '';

  /**
   * @param string $template
   */
  public function setTemplate(string $template) {
    $this->template = $template;
  }

  /**
   * @param array $variables
   */
  public function setVariables(array $variables) {
    $this->variables = $variables;
  }

  /**
   * @return string
   */
  public function getOutput(): string {
    return $this->output;
  }

  /**
   * @param string $output
   */
  public function setOutput(string $output) {
    $this->output = $output;
  }
}
