<?php

namespace PatternTwig\Pattern;

use PatternTwig\Pattern\Event\IncludeEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Twig extension providing 'pattern()' function.
 *
 * @package PatternTwig\Template
 */
class TwigExtension extends \Twig_Extension {

  /**
   * Symfony event dispatcher instance.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $dispatcher;

  /**
   * TwigExtension constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   Symfony event dispatcher.
   */
  public function __construct(EventDispatcherInterface $dispatcher) {
    $this->dispatcher = $dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'pattern_twig_extension';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('pattern', function (\Twig_Environment $env, $context, $template, $variables = [], $withContext = true, $ignoreMissing = false, $sandboxed = false) {

        $include = new IncludeEvent($template, $variables);
        $this->dispatcher->dispatch(IncludeEvent::PRE_INCLUDE, $include);
        $template = $include->getTemplate();
        $variables = $include->getVariables();

        // For full compatibility with 'pattern()' implementations like Drupal
        // UI Patterns, make sure template has .twig extension. This can be an
        // issue with 'pattern('button')' if template has .html.twig suffix.
        // TODO: provide a custom loader to fix issue above.
        $extension = '.twig';
        if (strlen($template) < strlen($extension) || strpos($template, $extension, -5) === FALSE) {
          $template .= $extension;
        }

        $output = twig_include($env, $context, $template, $variables, $withContext, $ignoreMissing, $sandboxed);

        $include->setOutput($output);
        $this->dispatcher->dispatch(IncludeEvent::POST_INCLUDE, $include);

        return $include->getOutput();
      }, ['needs_environment' => true, 'needs_context' => true, 'is_safe' => ['all']]),
    ];
  }

}
